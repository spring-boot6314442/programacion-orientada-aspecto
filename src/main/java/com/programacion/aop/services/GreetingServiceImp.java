package com.programacion.aop.services;

import org.springframework.stereotype.Service;

@Service
public class GreetingServiceImp implements IGreetingSevice {
    @Override
    public String sayHello(String person, String phrase) {
        String greeting = phrase + " " + person;
        System.out.println(greeting);
        return greeting;
    }

    @Override
    public String titleMethod(String title) {
        throw new RuntimeException("algún error");

    }
}

package com.programacion.aop.services;

public interface IGreetingSevice {
    String sayHello(String person, String phrase);
    String titleMethod(String title);
}

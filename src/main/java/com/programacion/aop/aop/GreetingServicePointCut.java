package com.programacion.aop.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class GreetingServicePointCut {
    @Pointcut("execution(String com.programacion.aop.services.IGreetingSevice.*(..))")
    public void greetingLoggerFooPointCut(){
    }
}

package com.programacion.aop.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Arrays;
@Order(2)
@Aspect
@Component
public class GreetingAspect {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Pointcut("execution(String com.programacion.aop.services.IGreetingSevice.*(..))")
    private void greetingLoggerPointCut(){

    }

    @Before("greetingLoggerPointCut()")
    public void loggerBeforePointCut(JoinPoint joinPoint){
        String method = joinPoint.getSignature().getName();
        String args = Arrays.toString(joinPoint.getArgs());
        logger.info("antes del metodo " + method + " con los argumentos " + args + " usando pointcut");
    }
    @Before("execution(String com.programacion.aop.services.IGreetingSevice.*(..))")
    public void loggerBefore(JoinPoint joinPoint){
        String method = joinPoint.getSignature().getName();
        String args = Arrays.toString(joinPoint.getArgs());
        logger.info("antes del metodo " + method + " con los argumentos " + args);
    }

    @After("execution(String com.programacion.aop.services.IGreetingSevice.*(..))")
    public void loggerAfter(JoinPoint joinPoint){
        String method = joinPoint.getSignature().getName();
        String args = Arrays.toString(joinPoint.getArgs());
        logger.info("despues del metodo " + method + " con los argumentos " + args);
    }

    @AfterReturning("execution(String com.programacion.aop.services.*.*(..))")
    public void loggerAfterReturning(JoinPoint joinPoint){
        String method = joinPoint.getSignature().getName();
        String args = Arrays.toString(joinPoint.getArgs());
        logger.info("despues de retornar " + method + " con los argumentos " + args);
    }

    @AfterThrowing("execution(String com.programacion.aop.services.*.*(..))")
    public void loggerAfterThrowing(JoinPoint joinPoint){
        String method = joinPoint.getSignature().getName();
        String args = Arrays.toString(joinPoint.getArgs());
        logger.info("despues de lanzar la excepcion " + method + " con los argumentos " + args);
    }

    @Around("execution(String com.programacion.aop.services.*.*(..))")
    public Object loggerAround(ProceedingJoinPoint joinPoint) throws Throwable{
        String method = joinPoint.getSignature().getName();
        String args = Arrays.toString(joinPoint.getArgs());
        Object result = null;
        try{
            logger.info("el metodo " + method + " con los parametro " + args);
             result = joinPoint.proceed();
            logger.info("el metodo " + method + " retorna el resultado " + result);
            return  result;
        } catch (Throwable e){
            logger.error("error en la llamada del metodo " + method + "()");
            throw e;
        }
    }
}

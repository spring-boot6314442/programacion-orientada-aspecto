package com.programacion.aop.controllers;

import com.programacion.aop.services.IGreetingSevice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

@RestController
@RequestMapping("/api")
public class GreetingController {
    @Autowired
    private IGreetingSevice greetingSevice;

    @GetMapping("/greeting")
    public ResponseEntity<?> greeting() {
        return ResponseEntity.ok(Collections.singletonMap("greeting", greetingSevice.sayHello("pepe", "hello word")));
    }

    @GetMapping("/method")
    public ResponseEntity<?> greetingTitle(){
        return ResponseEntity.ok(Collections.singletonMap("title", greetingSevice.titleMethod("AfterThrowing")));
    }
}
